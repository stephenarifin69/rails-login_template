class RegistrationsController < Devise::RegistrationsController

  before_filter :configure_permitted_parameters

  # Path redirect after a user registers
  def after_sign_up_path_for(resource)
    new_user_session_path
  end

  protected
    # Add additional registration fields for sign-up
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) do |u|
        u.permit(:email, :password, :password_confirmation)
      end
    end

end